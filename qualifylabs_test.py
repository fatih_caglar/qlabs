import json
import unittest
import argparse
import sys

class BaseTest(unittest.TestCase):
    def __init__(self, testName, filename):
        super(BaseTest, self).__init__(testName)          
        self.filename = filename

    def setUp(self):
        with open(self.filename ) as data:
            self.data = json.load(data)

    def print_errors(self,err_type):
        err_msg = "Empty records" if err_type == "empty" else "Missing Fields"

        if self.defects['books'][err_type] or self.defects['authors'][err_type]:
            for t in self.types:
                self.msg += "\n::: "+ err_msg +" of " + t + " :::\n "
                for d in self.defects[t][err_type]:
                    self.msg += "->  " + json.dumps(d) + "\n"


    # All authors are NOT expected to have a book in the data set, but all books SHOULD have valid authors.
    def test_itegrity(self):
        
        msg = "" 
        
        valid_author_list = []
        for author in self.data['authors']:
            try: 
                if author['id'] and author['name']:
                    valid_author_list.append(author['id'])
            except KeyError:
                pass
        non_valid_books = []
        
        for book in self.data['books']:
            try:
                if not book['author'] in valid_author_list:
                    non_valid_books.append(book)
            except KeyError:
                non_valid_books.append(book)
        if non_valid_books:
            msg += "\n::: Book objects that can break data integrity :::\n"
            for nvb in non_valid_books:
                msg += json.dumps(nvb) + "\n"

        self.assertFalse(non_valid_books, msg=msg)
   
    # check empty and non-existed fields
    def check_fields(self,obj_type,row):
        try:
            is_empty = not bool(row['id'] and row['name'])
            if obj_type == "books":
                is_empty = is_empty or not bool(row['author'])

            status = "empty" if is_empty else "OK"
        except KeyError:
            status = "non_existent"
        finally:
            return status
    
    def test_empty_and_non_existent_fields(self):
        self.types = ('books', 'authors')
        self.defects = { 'authors': {'empty': [], 'non_existent': [] }, 'books'  : {'empty': [], 'non_existent': [] } }

        for t in self.types:
            for row in self.data[t]:
                row_status = self.check_fields(t,row)
                if row_status is not "OK":
                    self.defects[t][row_status].append(row)
        
        self.msg = ""
        self.print_errors("empty")
        self.print_errors("non_existent")
        
        self.assertFalse(bool(self.defects['books']['empty'] or self.defects['books']['non_existent'] or  
                    self.defects['authors']['empty'] or self.defects['authors']['non_existent']),msg=self.msg)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        #sys.exit('ERROR filename parameter must be supplied for these tests eg: "book_data.json" ')
        filename = "book_data.json"
    elif len(sys.argv) == 2: 
        filename = sys.argv.pop()
    
    suite = unittest.TestSuite()
    suite.addTest(BaseTest("test_itegrity",filename))
    suite.addTest(BaseTest("test_empty_and_non_existent_fields",filename))
    unittest.TextTestRunner(verbosity=2).run(suite)

